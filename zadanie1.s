@@@ OPEN INPUT FILE, READ IT, PRINT IT TO STDOUT

.equ SWI_Open, 0x66         @open a file 
.equ SWI_Close,0x68         @close a file
.equ SWI_PrChr,0x00         @ Write an ASCII char to Stdout 
.equ SWI_PrStr, 0x69        @ Write a null-ending string 
.equ SWI_PrInt,0x6b         @ Write an Integer 
.equ SWI_RdInt,0x6c         @ Read an Integer from a file 
.equ SWI_RdStr,0x6a 		@ Read an String from a file
.equ Stdout, 1         		@ Set output target to be Stdout 
.equ SWI_Exit, 0x11         @ Stop execution 
.global _start 
.text
_start:
@ print an initial message to the screen 
mov R0,#Stdout          @print an initial message 
ldr R1, =Message1       @ load address of Message1 label 
swi SWI_PrStr           @ display message to Stdout 

@ == Open an input file for reading =============================

@ if problems, print message to Stdout and exit 
ldr r0,=InFileName      @ set Name for input file 
mov r1,#0           @ mode is input
swi SWI_Open            @ open file for input 
bcs InFileError         @ Check Carry-Bit (C): if= 1 then ERROR
@ Save the file handle in memory: 
ldr r1,=InFileHandle         @ if OK, load input file handle 
str r0,[r1]                 @ save the file handle 

@ == Read STRINGS until end of file =============================

OuterLoop: 
ldr r0,=InFileHandle     @ load input file handle 
ldr r0,[r0] 
ldr r1,=Clipboard
mov r2, #80
swi SWI_RdStr          @ read the integer into R0 
bcs EofReached          @ Check Carry-Bit (C): if= 1 then EOF reached 

	mov R0,#Stdout     		@print what is in clipboard on screen
	ldr r1, =Clipboard 
	swi SWI_PrStr	

mov R0,#Stdout          @ print new line 
ldr r1, =NL 
swi SWI_PrStr 
bal OuterLoop           @ keep reading till end of file

@ == End of file ===============================================

EofReached: 
mov R0, #Stdout         @ print last message 
ldr R1, =EndOfFileMsg 
swi SWI_PrStr

@ == Close files ===============================================
ldr R0, =InFileHandle       @ get address of file handle 
ldr R0, [R0]            @ get value at address 
swi SWI_Close

Exit: 
swi SWI_Exit            @ stop executing 
InFileError: 
mov R0, #Stdout 
ldr R1, =FileOpenInpErrMsg 
swi SWI_PrStr 
bal Exit            @ give up, go to end 

.data 
.align
Clipboard:			.skip 		80
InFileHandle:       .word       0 
InFileName:         .asciz      "whatever.txt"
FileOpenInpErrMsg:  .asciz      "Failed to open input file \n" 
EndOfFileMsg:       .asciz      "The End. \n"  
NL:             	.asciz      "\n"   @ new line 
Message1:       	.asciz      "This program reades whatever.txt and prints it in stdout \n" 
.end